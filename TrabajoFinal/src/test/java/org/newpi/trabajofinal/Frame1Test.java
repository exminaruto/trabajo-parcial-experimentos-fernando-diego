/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.newpi.trabajofinal;

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Fponce
 */
public class Frame1Test {
    
    
   
    @Before
    public void InicioMetodo()
    {
        System.out.println("Inicio del Metodo");
    }
    @After
    public void FinMetodo()
    {
        System.out.println("Fin del Metodo");
    }
    
    @Test
    public void testRegistrarPaciente() {
        try{
        System.out.println("Registrar Paciente");
        String num = "175268";
        String nom = "Fernando";
        String surna = "Chavez";
        String dir = "18 Julio C.Tello";
        String codser = "REA";
        String telf = "989 175 195";
        String room = "101";
        String Mut = "";
        String cama = "4";
        Frame1 instance = new Frame1();
        boolean Response = instance.RegistrarPaciente(num, nom, surna, dir, codser, telf, room, Mut, cama);
        assertTrue(Response == false);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testRegistrarEnfermera() {
        try{
        System.out.println("Registrar Enfermera");
        String num = "16";
        String nom = "Diego";
        String surna = "Guevara";
        String dir = "15 Av.Marina";
        String telf = "952 356 521";
        String codser = "REA";
        String rotacion = "JOUR";
        String salario = "1052.12";
        Frame1 instance = new Frame1();
        boolean Response = instance.AgregarEnfermera(num, nom, surna, dir, telf, codser, rotacion, salario);
        assertTrue(Response == false);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail();
        }
    }
    
}
